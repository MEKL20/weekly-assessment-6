function dropRight(arr, n = 1) {
  if (arr.length < n) return [];

  for (let i = 0; i < n; i++) {
    arr.pop();
  }

  return arr;
}

console.log(dropRight([1, 2, 3]));
console.log(dropRight([1, 2, 3], 2));
console.log(dropRight([1, 2, 3], 5));
console.log(dropRight([1, 2, 3], 0));
