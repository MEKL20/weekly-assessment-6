function sumOfCubes(arr) {
  if (arr.length == 0) return 0;

  let result = 0;
  for (let i = 0; i < arr.length; i++) {
    result += arr[i] ** 3;
  }
  return result;
}

console.log(sumOfCubes([1, 5, 9]));
console.log(sumOfCubes([3, 4, 5]));
console.log(sumOfCubes([2]));
console.log(sumOfCubes([]));
